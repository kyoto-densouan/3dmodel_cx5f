# README #

1/3スケールのヤマハ CX5F風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- ヤマハ

## 発売時期
- 1984年

## 類似モデル
- YIS503
- Victor HC-6

## 参考資料

- [ホーム製品情報シンセサイザー・音楽制作CX5F](https://jp.yamaha.com/products/music_production/music-computers/cx5f/index.html)
- [ボクたちが愛した、想い出のレトロパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1201809.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cx5f/raw/126648432df5785b7532c14b20678c0a005443fc/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cx5f/raw/126648432df5785b7532c14b20678c0a005443fc/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cx5f/raw/126648432df5785b7532c14b20678c0a005443fc/ExampleImage.png)
